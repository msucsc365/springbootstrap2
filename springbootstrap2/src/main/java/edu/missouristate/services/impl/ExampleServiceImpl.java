package edu.missouristate.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.dao.ExampleRepository;
import edu.missouristate.model.Credentials;
import edu.missouristate.services.ExampleService;

@Service("exampleService") 
public class ExampleServiceImpl implements ExampleService {

	@Autowired
	ExampleRepository exampleRepo;
	
	@Override
	public boolean isAuthenticated(String id, String password) {
		Credentials creds = exampleRepo.getCredentialsList(id, password);
		return creds != null;
	}

}
